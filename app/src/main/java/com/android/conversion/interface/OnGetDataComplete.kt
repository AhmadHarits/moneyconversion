package com.android.conversion.`interface`

import com.android.conversion.network.DownloadStatus

interface OnGetDataComplete {
    fun onGetDataComplete(data: String, status: DownloadStatus)
}